package main

import (
	"bytes"
	"fmt"
	"image"
	"image/gif"
	_ "image/jpeg"
	_ "image/png"
	"log"
	"os"
	"strconv"
)

func main() {
	args := os.Args
	var files []string

	var frames []*image.Paletted
	var delay int = 3
	var output string = "out.gif"

	if len(args) < 2 {
		log.Printf("No argument input")
		os.Exit(1)
	}

	// check arguments
	for i := 0; i < len(args); i++ {
		if i == 0 {
			continue
		}
		if args[i] == "-o" {
			i++
			output = args[i]
			log.Printf("Output file name: %s", output)
			continue
		}
		if args[i] == "-d" {
			i++
			s, err := strconv.Atoi(args[i])
			delay = s
			if err != nil {
				log.Printf("-d requires an integer")
				os.Exit(1)
			}
			log.Printf("Frame delay: %d", delay)
			continue
		}
		files = append(files, args[i])
		log.Printf("file: %s", args[i])
	}

	// printSlice(files)

	if len(files) < 1 {
		log.Printf("No file input")
		os.Exit(1)
	}

	for i, filename := range files {
		img, err := os.Open(filename)
		if err != nil {
			log.Printf("Unable to read file %s: %s", filename, err)
			os.Exit(1)
		}

		log.Printf("Parsing image %d of %d : %s", i, len(files), filename)

		src, _, err := image.Decode(img)
		if err != nil {
			log.Printf("Unable to decode file %s: %s", filename, err)
			os.Exit(1)
		}

		bytebuf := bytes.Buffer{}
		if err := gif.Encode(&bytebuf, src, nil); err != nil {
			log.Printf("Unable to encode image %s to gif: %s", filename, err)
			os.Exit(1)
		}

		gif, err := gif.Decode(&bytebuf)
		if err != nil {
			log.Printf("Unable to decode gif at image %s: %s", filename, err)
			os.Exit(1)
		}

		log.Printf("Parsed all images...creating animated GIF")

		frames = append(frames, gif.(*image.Paletted))
	}

	delays := make([]int, len(frames))
	for i, _ := range delays {
		delays[i] = delay
	}

	out, err := os.Create("out.gif")
	if err != nil {
		log.Printf("Unable to create destination file: %s", err)
		os.Exit(1)
	}

	outGif := &gif.GIF{}
	outGif.Image = frames
	outGif.Delay = delays
	if err := gif.EncodeAll(out, outGif); err != nil {
		log.Printf("Unable to encode output into animated gif: %s", err)
		os.Exit(1)
	}

	out.Close()
}

func printSlice(s []string) {
	fmt.Printf("len=%d cap=%d %s\n", len(s), cap(s), s)
}
